import markov as markovo
import random

# xtext munger from Eleonora


# ${\small \textnormal{\useTextGlyph{fxl}{element}}}$
def xtext(chunks):
    xtext = markovo.Markov(chunks)
    ytext = markovo.Markov(chunks)
    chunk = random.choice(chunks)
    return "<p>" + xtext.generate_markov_text() + chunk.text + \
		ytext.generate_markov_text() + "</p>"


