from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import ForeignKey, Column, Integer, UnicodeText, String, Boolean
from sqlalchemy.orm import relation, backref
import dulwich

filestore = "corpora"

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'
    
    id = Column(Integer, primary_key=True)
    name = Column(String)
    password = Column(String)
    
    def __init__(self, name, password):
        self.name = name
        self.password = password
        
    def __repr__(self):
        return "<User('%s')>" % (self.name)


class Corpus(Base):
    __tablename__ = 'corpora'
    
    id = Column(Integer, primary_key=True)
    name = Column(String)
    creator_id = Column(Integer, ForeignKey('users.id'))
    creator = relation(User, backref=backref('corpora', order_by=id))
    current_round_index = Column(Integer)
    total_rounds = Column(Integer)
    max_participants = Column(Integer)
    
    #TODO: use id as repo name
    
    def __init__(self, name, creator, total_rounds=10):
        self.name = name
        self.creator = creator
        self.current_round_index = 1
        self.total_rounds = total_rounds
        
    def __repr__(self):
        return "<Corpus('%s','%s','%s','%s')>" % (self.name, self.creator, 
                self.current_round_index, self.total_rounds)
        
        
class Round(Base):
    __tablename__ = 'rounds'
    
    id = Column(Integer, primary_key=True)
    text_out = Column(UnicodeText)
    author_id = Column(Integer, ForeignKey('users.id'))
    author = relation(User, backref=backref('rounds', order_by=id))
    corpus_id = Column(Integer, ForeignKey('corpora.id'))
    corpus = relation(Corpus, backref=backref('rounds', order_by=id))
    round_index = Column(Integer)
    length_seconds = Column(Integer)
    algorithm = Column(String)
    
    def __init__(self, corpus, length_seconds="360", algorithm="roundrobin"):
        self.corpus = corpus
        self.round_index = corpus.current_round_index
        self.length_seconds = length_seconds
        self.algorithm = algorithm
        
    def __repr__(self):
        return "<Round('%s','%s','%s','%s','%s')>" % (self.author, 
                self.corpus, self.length_seconds, self.algorithm, 
                self.text_out)

        
class Chunk(Base):
    __tablename__ = 'chunks'
    
    id = Column(Integer, primary_key=True)
    text = Column(UnicodeText)
    author_id = Column(Integer, ForeignKey('users.id'))
    author = relation(User, backref=backref('chunks', order_by=id))
    corpus_id = Column(Integer, ForeignKey('corpora.id'))
    corpus = relation(Corpus, backref=backref('chunks', order_by=id))
    round_id = Column(Integer, ForeignKey('rounds.id'))
    round = relation(Round, backref=backref('chunks', order_by=id))
    antecedent_id = Column(Integer, ForeignKey('chunks.id'))
    descendent = relation("Chunk", backref=backref("antecedent", uselist=False, remote_side=[id]))
    
    def __init__(self, text, author, corpus, round):
        self.text = text
        self.author = author
        self.corpus = corpus
        self.round = round
        
    def __repr__(self):
        return "<Chunk('%s','%s','%s','%s','%s')>" % (self.author, self.round, 
                self.corpus, self.text, self.antecedent, self.descendent)
        
        

