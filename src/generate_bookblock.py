#!/usr/bin/env python
# generate the latex based book block for a maggot corpus

from model import Chunk, Base

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

latex_header = open("templates/latex-header.tex", "r").read()
latex_footer = open("templates/latex-footer.tex", "r").read()

latex_out = open("/tmp/excode_book_out.tex", "w")

entitylatexmap = [("&nbsp;", " "),
                  ("\\", "\\textbackslash "),
                  ("#", "\\# "),
                  ("$", "\\$ "),
                  ("%", "\\% "),
                  ("~", "\\~{} "),
                  ("^", "\\^{} "),
                  ("_", "\\_ "),
                  ("{", "\\{ "),
                  ("}", "\\} "),
                  ("&gt;", "$>$"),
                  ("&lt;", "$<$"),
                  ("&plusmn;", "$\\pm$"),
                  ("&infin;", "$\\infty$"),
                  ("&eacute;", "\\'{e} "),
                  ("&ugrave;", "\\`{u} "),
                  ("\n", "\n\n")]

symbol_list = ["\\alpha", "\\beta", "\\gamma", "\\delta",
               "\\epsilon", "\\zeta", "\\eta", "\\theta",
               "\\iota", "\\kappa", "\\lambda", "\\mu",
               "\\nu", "\\xi", "\\omicron", "\\pi", "\\rho",
               "\\sigma", "\\tau", "\\upsilon", "\\phi",
               "\\chi", "\\psi", "\\omega"]

author_symbols = {}


def span_thread(sess, chunk):
    #print "----fork----"
    #print chunk.id
    text = chunk.text
    if not(chunk.author.id in author_symbols):
        author_symbols[chunk.author.id] = symbol_list.pop()
        print "author: " + chunk.author.name + " : " + author_symbols[chunk.author.id]
    author = "$" + author_symbols[chunk.author.id] + "$"
    #TODO: fake more symbols when we have too many authors
    for entity in entitylatexmap:
        text = text.replace(entity[0], entity[1]) + "\n"
    #print text
    text = "\\hspace{0pt}\\marginpar[\\hfill" + author + "]{" + author + "}" + text
    latex_out.write(text)
    forks = sess.query(Chunk).filter(Chunk.antecedent == chunk).all()
    if len(forks) == 0:
        #print "----end of thread----"
        return
    elif len(forks) > 1:
        #print "chunk has " + str(len(forks)) + " children"
        pass
    for fork in forks:
        span_thread(sess, fork)

sqlengine = create_engine('sqlite:///excode-edinburgh.db')
SqlSession = sessionmaker()
sqlmeta = Base.metadata
sqlmeta.create_all(sqlengine)
SqlSession.configure(bind=sqlengine)
sess = SqlSession()

starts = sess.query(Chunk).filter(Chunk.antecedent == None).all()
#print "total story threads: " + str(len(starts))

latex_out.write(latex_header)
chapter = 0
for start in starts:
    latex_out.write("\\chapter*{Thread " + str(chapter) + "}\n")
    latex_out.write("\\addcontentsline{toc}{chapter}{Thread " + str(chapter) + "}\n")
    latex_out.write("\\pagestyle{fancy}\n")
    span_thread(sess, start)
    chapter += 1

latex_out.write(latex_footer)
